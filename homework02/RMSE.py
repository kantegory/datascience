from GDRegressor import GDRegressor
import pandas as pd


def rmse(y_hat, y):
    """
    Считаем среднеквадратичную ошибку
    :param y_hat:
    :param y: вектор прогнозов, сформированный в predict
    :return: среднеквадратичная ошибка
    """
    m = y.size  # считаем размер выборки
    RMSE = 0  # будущая среднеквадратичная ошибка
    for i in range(m):
        """
        Считаем среднеквадратичную ошибку
        """
        RMSE = ((sum(y_hat[i] - y[i]) ** 2) / m) ** 0.5
    return RMSE


def r_squared(y_hat, y):
    """
    Считаем коэффициент детерминации
    :param y_hat:
    :param y: вектор прогнозов, сформированный в predict
    :return: коэффициент детерминации
    """
    m = y.size  # считаем размкр выборки
    DETERMINATION_COEF = 0  # будущий коэффициент детерминации
    for i in range(m):
        """
        Считаем коэффициент детерминцаии
        """
        DETERMINATION_COEF = 1 - (sum((y[i] - y_hat[i]) ** 2) / (sum((y[i] - y.mean()) ** 2)))
    return DETERMINATION_COEF


if __name__ == '__main__':
    df = pd.read_csv('brain_size.csv')
    X = df.iloc[:, 1:2]
    Y = df['VIQ'].as_matrix()
    model = GDRegressor()
    model.fit(X, Y)
    y = model.predict(X)
    rmse(y, Y)
    r_squared(y, Y)
